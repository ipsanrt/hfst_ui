import argparse

from app import app

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="HOST", default="0.0.0.0")
    parser.add_argument("--port", type=int, help="PORT", default=8080)
    parser.add_argument("--debug", help="DEBUG", default=False)
    args = parser.parse_args()

    app.run(host=args.host, port=args.port, debug=args.debug)
