"""empty message

Revision ID: e89bca73b27a
Revises: 0051dc34b9a0
Create Date: 2020-03-14 22:02:34.971306

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e89bca73b27a"
down_revision = "0051dc34b9a0"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("transducers", sa.Column("release_note", sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("transducers", "release_note")
    # ### end Alembic commands ###
