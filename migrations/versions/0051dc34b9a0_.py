"""empty message

Revision ID: 0051dc34b9a0
Revises: 69b90aaa8e84
Create Date: 2020-03-14 20:38:43.004500

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0051dc34b9a0"
down_revision = "69b90aaa8e84"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "transducers", sa.Column("release_date", sa.DateTime(), nullable=True)
    )
    op.add_column("transducers", sa.Column("user_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        None,
        "transducers",
        "ab_user",
        ["user_id"],
        ["id"],
        onupdate="CASCADE",
        ondelete="CASCADE",
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "transducers", type_="foreignkey")
    op.drop_column("transducers", "user_id")
    op.drop_column("transducers", "release_date")
    # ### end Alembic commands ###
