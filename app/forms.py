from flask import request
from wtforms import Form, StringField, TextField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from flask_appbuilder.fieldwidgets import (
    BS3TextFieldWidget,
    BS3TextAreaFieldWidget,
    Select2Widget,
)
from flask_appbuilder.forms import DynamicForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flask_babel import lazy_gettext as _

from . import appbuilder, db
from .models import Transducer


class HFSTCompileForm(DynamicForm):
    lexc_text = TextField(
        _("LEXC File"),
        description=_("Morphological rules"),
        validators=[DataRequired()],
        widget=BS3TextAreaFieldWidget(),
    )
    twolc_text = TextField(
        _("TWOLC File"),
        description=_("Phonological rules"),
        validators=[DataRequired()],
        widget=BS3TextAreaFieldWidget(),
    )


class HFSTAnalyseForm(DynamicForm):
    version = QuerySelectField(
        _("Version"),
        query_factory=lambda: db.session.query(Transducer).all(),
        widget=Select2Widget(),
    )
    word = TextField(
        _("Token"),
        description=_("Write just one word(=token) without any additional symbols"),
        validators=[DataRequired()],
        widget=BS3TextFieldWidget(),
    )


class AddTestCasesForm(DynamicForm):
    file = FileField(
        _("File"),
        description=_("Upload your text file with test cases"),
        validators=[DataRequired()],
    )


class RunTestForm(DynamicForm):
    version = QuerySelectField(
        _("Version"),
        query_factory=lambda: db.session.query(Transducer).all(),
        widget=Select2Widget(),
    )
    filter_tags = TextField(
        _("Filter Tags"),
        description=_(
            "Use comma to separate values. If empty, no filter will be applied"
        ),
        widget=BS3TextFieldWidget(),
    )


class ReleaseForm(DynamicForm):
    tag = TextField(
        _("Tag"),
        description=_("Write version or tag, e.g. "),
        validators=[DataRequired()],
        widget=BS3TextFieldWidget(),
    )
    release_note = TextField(
        _("Release Note"),
        description=_("Shortly describe the changes"),
        validators=[DataRequired()],
        widget=BS3TextAreaFieldWidget(),
    )


class TestCaseAddForm(FlaskForm):
    """                                                                                                                                                                                                                             
    A custom FlaskForm which reads data from request params                                                                                                                                                                         
    """

    @classmethod
    def refresh(cls, obj=None):
        kw = dict(obj=obj)
        if request.method == "GET":
            kw["formdata"] = request.args
        form = cls(**kw)
        return form
