# -*- coding: utf-8 -*-
import re
import datetime
import logging
import uuid

import requests
from flask import render_template, url_for, redirect, flash, request
from flask_appbuilder import BaseView, ModelView, SimpleFormView, expose, has_access
from flask_appbuilder.actions import action
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder.security.sqla.models import User
from flask_appbuilder.filemanager import FileManager
from flask_babel import lazy_gettext as _
import psycopg2
import libhfst
from sqlalchemy.exc import IntegrityError
from sqlalchemy import any_

from . import appbuilder, db
from .models import TestCase, Transducer
from .forms import (
    HFSTCompileForm,
    HFSTAnalyseForm,
    RunTestForm,
    ReleaseForm,
    TestCaseAddForm,
    AddTestCasesForm,
)
from .hfst.dev import (
    run_console_code,
    compile,
    analyse,
    copy_hfst,
)


def get_primary_admin():
    # check if user.id = 1 is created
    # if not, create admin user
    try:
        user = db.session.query(User).filter(User.username == "admin").first()
        if not user:
            raise Exception("ADMIN user does not exists!")
        return user

    except Exception as e:
        logging.error(e)
        try:
            role_admin = appbuilder.sm.find_role(appbuilder.sm.auth_role_admin)
            appbuilder.sm.add_user(
                "admin", "admin", "admin", "admin@fab.org", role_admin, "admin"
            )
            user = db.session.query(User).filter(User.username == "admin").first()
            return user

        except Exception as e:
            logging.error(e)


def get_dev_transducer():
    try:
        dev_transducer = (
            db.session.query(Transducer).filter(Transducer.tag == "DEV").first()
        )
        if not dev_transducer:
            raise Exception("DEV Transducer does not exists!")
        return dev_transducer

    except Exception as e:
        logging.error(e)
        try:
            dev_transducer = Transducer(
                tag="DEV",
                lexc_text="--- fill with rules --",
                twolc_text="--- fill with rules --",
                hfst_file="dev.hfst",
                user=get_primary_admin(),
                release_note="!! DEV version keeps changing everytime when compiled !!",
            )
            db.session.add(dev_transducer)
            db.session.commit()
            return dev_transducer

        except Exception as e:
            logging.error(e)


def run_test(hfst_file, tags):
    if not tags:
        test_cases = db.session.query(TestCase).all()
    else:
        test_cases = (
            db.session.query(TestCase)
            .filter(
                TestCase.filter_tags.like(
                    any_(["%" + tag.strip() + "%" for tag in tags.split(",")])
                )
            )
            .distinct()
            .all()
        )

    succeed = 0
    all = len(test_cases)
    error_cases = []
    for test_case in test_cases:
        result = analyse(test_case.word, hfst_file)
        if result == test_case.result:
            succeed += 1
        else:
            error_cases.append((test_case, result))
    return succeed, all, error_cases

def uuid_namegen(filename):
    return str(uuid.uuid1()) + "_sep_" + filename

class HFSTCompileView(SimpleFormView):
    form = HFSTCompileForm
    form_title = _("HFST Compile")
    message = _("Compiled successfully to DEV version!")

    def form_get(self, form):
        transducer = get_dev_transducer()
        tag = request.args.get("tag")
        logging.debug("*** TAG: %s" % tag)
        if tag:
            transducer = (
                db.session.query(Transducer).filter(Transducer.tag == tag).first()
                or transducer
            )
        logging.debug("*** Transducer: %s" % transducer)
        form.lexc_text.data = transducer.lexc_text
        form.twolc_text.data = transducer.twolc_text

    def form_post(self, form):
        try:
            filename = uuid_namegen("dev.hfst")
            res, stdout = compile(filename, form.lexc_text.data, form.twolc_text.data)
            if not res:
                if isinstance(stdout, bytes):
                    stdout = stdout.decode("utf-8").replace("\n", " ")
                flash(stdout, "danger")
                return redirect(url_for("HFSTCompileView.this_form_get"))
            else:
                # save current code to DB
                dev_transducer = get_dev_transducer()
                dev_transducer.lexc_text = form.lexc_text.data
                dev_transducer.twolc_text = form.twolc_text.data
                dev_transducer.release_date = datetime.datetime.utcnow()
                dev_transducer.hfst_file = filename
                db.session.commit()

                # redirect to analyse page
                flash(self.message, "success")
                return redirect(url_for("HFSTAnalyseView.this_form_get"))

        except Exception as e:
            logging.error(e)
            flash(str(e), "danger")
            return redirect(url_for("HFSTCompileView.this_form_get"))


appbuilder.add_view(
    HFSTCompileView,
    "Compile",
    label=_("Compile"),
    icon="fa-cogs",
    category="Development",
    category_label=_("Development"),
    category_icon="fa-codepen",
)


class HFSTAnalyseView(SimpleFormView):
    form = HFSTAnalyseForm
    form_title = _("HFST Analyse")

    def form_get(self, form):
        transducer = get_dev_transducer()
        tag = request.args.get("tag")
        logging.debug("*** TAG: %s" % tag)
        if tag:
            transducer = (
                db.session.query(Transducer).filter(Transducer.tag == tag).first()
                or transducer
            )
        logging.debug("*** Transducer: %s" % transducer)
        form.version.data = transducer
        form.word.data = request.args.get("word")

    def form_post(self, form):
        try:
            result = analyse(form.word.data, form.version.data.hfst_file)
            marked_result = result.replace(";", "<br/>")
            return self.render_template(
                "analyse_result.html",
                word=form.word.data,
                result=result,
                tag=form.version.data.tag,
                marked_result=marked_result,
            )

        except libhfst.NotTransducerStreamException as e:
            logging.error(e)
            flash(_("HFST is not compiled properly! Please recompile!"), "danger")
            return redirect(
                url_for("HFSTAnalyseView.this_form_get", tag=form.version.data.tag)
            )

        except Exception as e:
            logging.error(e)
            flash(str(e), "danger")
            return redirect(
                url_for("HFSTAnalyseView.this_form_get", tag=form.version.data.tag)
            )


appbuilder.add_view(
    HFSTAnalyseView, "Analyse", label=_("Analyse"), icon="fa-terminal",
)


class AddTestCasesFileView(SimpleFormView):
    form = AddTestCasesForm
    form_title = _("Add Test Cases as a File")

    def form_post(self, form):
        try:
            # save file into UPLOADS/ folder
            fm = FileManager(allowed_extensions=["txt", "csv"])
            if not fm.is_file_allowed(form.file.data.filename):
                raise Exception(
                    _("Upload only *.txt or *.csv. Your file is not allowed: ")
                    + str(form.file.data.filename)
                )

            try:
                data = form.file.data
                for row in data.readlines():
                    row = row.decode("utf-8").strip("\n")
                    token = row.split(",")
                    if len(token) < 2:
                        continue
                    test_case = db.session.query(TestCase).filter(TestCase.word == token[0]).filter(TestCase.result == token[1]).first()
                    if test_case:
                        if len(token) > 2:
                            test_case.filter_tags = ",".join(list(set(token[2:] + (test_case.filter_tags or "").split(","))))
                    else:
                        test_case = TestCase(word=token[0], result=token[1], filter_tags=",".join(token[2:]) if len(token) > 2 else None)
                        db.session.add(test_case)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                raise e

            return redirect(url_for("TestCaseView.list"))

        except Exception as e:
            logging.error(e)
            flash(str(e), "danger")
            return redirect(
                url_for("AddTestCasesFileView.this_form_get")
            )


appbuilder.add_view(
    AddTestCasesFileView,
    "Add Test Cases (File)",
    label=_("Add Test Cases (File)"),
    icon="fa-file",
    category="Test",
    category_label=_("Test"),
    category_icon="fa-quora",
)


class TestCaseView(ModelView):
    datamodel = SQLAInterface(TestCase)
    list_title = _("List of Test Cases")
    show_title = _("Test Case")
    add_title = _("Add Test Case")
    edit_title = _("Edit Test Case")
    label_columns = {
        "word": _("Word"),
        "result": _("Result"),
        "tags_list": _("Tag Set for Filtering"),
    }
    list_columns = ["word", "result", "tags_list"]
    description_columns = {
        "filter_tags": _(
            "Use tags to test model on different subsets. Use comma to divide tags"
        )
    }
    # install our very own form
    def _init_forms(self):
        super(TestCaseView, self)._init_forms()
        self.add_form = type("TestCaseAddForm", (TestCaseAddForm, self.add_form), {})

    @action("muldelete", _("Delete"), _("Delete all Really?"), "fa-eraser", single=True)
    def muldelete(self, items):
        if isinstance(items, list):
            self.datamodel.delete_all(items)
        else:
            self.datamodel.delete(items)
        return redirect(url_for("TestCaseView.list"))


appbuilder.add_view(
    TestCaseView,
    "Test Cases",
    label=_("Test Cases"),
    icon="fa-list",
    category="Test",
    category_label=_("Test"),
)


class RunTestView(SimpleFormView):
    form = RunTestForm
    form_title = _("Run Analyser on Test Cases")

    def form_get(self, form):
        transducer = get_dev_transducer()
        tag = request.args.get("tag")
        logging.debug("*** TAG: %s" % tag)
        if tag:
            transducer = (
                db.session.query(Transducer).filter(Transducer.tag == tag).first()
                or transducer
            )
        logging.debug("*** Transducer: %s" % transducer)
        form.version.data = transducer
        form.filter_tags.data = request.args.get("filter_tags")

    def form_post(self, form):
        try:
            succeed, all, error_cases = run_test(
                form.version.data.hfst_file, form.filter_tags.data
            )
            if all == 0:
                flash(
                    "There is no Test Cases with these tags: %s"
                    % form.filter_tags.data,
                    "warning",
                )
                return redirect(
                    url_for(
                        "RunTestView.this_form_get",
                        tag=form.version.data.tag,
                        filter_tags=form.filter_tags.data,
                    )
                )

            result = "%s out of %s passed the QA (accuracy = %.2f %%)" % (
                succeed,
                all,
                (succeed * 100.0 / all),
            )
            return self.render_template(
                "run_test_result.html",
                result=result,
                tag=form.version.data.tag,
                error_cases=error_cases,
                filter_tags=form.filter_tags.data,
            )

        except libhfst.NotTransducerStreamException as e:
            logging.error(e)
            flash(_("HFST is not compiled properly! Please recompile!"), "danger")
            return redirect(
                url_for(
                    "RunTestView.this_form_get",
                    tag=form.version.data.tag,
                    filter_tags=form.filter_tags.data,
                )
            )

        except Exception as e:
            logging.error(e)
            flash(str(e), "danger")
            return redirect(
                url_for(
                    "RunTestView.this_form_get",
                    tag=form.version.data.tag,
                    filter_tags=form.filter_tags.data,
                )
            )


appbuilder.add_view(
    RunTestView,
    "Run Test",
    label=_("Run Test"),
    icon="fa-terminal",
    category="Test",
    category_label=_("Test"),
)


class DEVVersionView(BaseView):
    route_base = "/dev"
    default_view = "show"

    @expose("/show")
    @has_access
    def show(self):
        return redirect(url_for("TransducerView.show", pk=get_dev_transducer().id))


appbuilder.add_view(
    DEVVersionView,
    "DEV Version",
    label=_("DEV Version"),
    icon="fa-star-o",
    category="Release",
    category_label=_("Release"),
    category_icon="fa-ravelry",
)


class TransducerView(ModelView):
    datamodel = SQLAInterface(Transducer)
    list_title = _("List of Transducer")
    show_title = _("Transducer")
    add_title = _("Add Transducer")
    edit_title = _("Edit Transducer")
    label_columns = {
        "tag": _("Version"),
        "date": _("Date"),
        "user": _("User"),
        "actions": _("Actions"),
        "release_note": _("Release Note"),
        "twolc_text": _("Twolc file content"),
        "lexc_text": _("Lexc file content"),
        "filter_tags": _("Filter Tags"),
        "release_date": _("Release Date"),
        "hfst_file": _("HFST file"),
    }
    list_columns = ["tag", "date", "user", "actions"]
    show_columns = [
        "tag",
        "date",
        "user",
        "release_note",
        "actions",
        "twolc_text",
        "lexc_text",
    ]
    base_permissions = ["can_list", "can_show", "can_delete", "can_download"]
    base_order = ("release_date", "desc")

    @action("muldelete", _("Delete"), _("Delete all Really?"), "fa fa-eraser", single=True)
    def muldelete(self, items):
        if isinstance(items, list):
            self.datamodel.delete_all(items)
        else:
            self.datamodel.delete(items)
        return redirect(url_for("TransducerView.list"))


appbuilder.add_view(
    TransducerView,
    "Versions",
    label=_("Versions"),
    icon="fa-list",
    category="Release",
    category_label=_("Release"),
)


class ReleaseView(SimpleFormView):
    form = ReleaseForm
    form_title = _("Release current DEV model as tagged version")

    def form_get(self, form):
        try:
            # for Postgres
            last_transducer = (
                db.session.query(Transducer)
                .filter(Transducer.tag.op("SIMILAR TO")(r"[0-9]+\.[0-9]+"))
                .order_by(Transducer.id.desc())
                .first()
            )
        except:
            # for MYSQL
            last_transducer = (
                db.session.query(Transducer)
                .filter(Transducer.tag.op("regexp")(r"[0-9]+\.[0-9]+"))
                .order_by(Transducer.id.desc())
                .first()
            )

        if last_transducer:
            major_version, minor_version = last_transducer.tag.split(".")
            minor_version = int(minor_version) + 1
        else:
            major_version = 0
            minor_version = 1
        form.tag.data = "%s.%s" % (major_version, minor_version)

    def form_post(self, form):
        try:
            dev_transducer = get_dev_transducer()
            new_hfst_file = uuid_namegen("transducer_v%s" % form.tag.data)
            copy_hfst(dev_transducer.hfst_file, new_hfst_file)

            new_transducer = Transducer(
                tag=form.tag.data,
                lexc_text=dev_transducer.lexc_text,
                twolc_text=dev_transducer.twolc_text,
                hfst_file=new_hfst_file,
                release_note=form.release_note.data,
            )
            db.session.add(new_transducer)
            db.session.commit()

            # redirect to analyse page
            flash(_("Successfully released!"), "success")
            # return redirect(url_for("TransducerView.list"))
            return redirect(url_for("TransducerView.show", pk=new_transducer.id))

        except IntegrityError as e:
            logging.error(e)
            flash(_("Release failed!"), "danger")
            if isinstance(e.orig, psycopg2.errors.UniqueViolation):
                flash(_("Tagged version already exists! Choose another tag!"), "danger")
            else:
                flash(str(e), "danger")
            return redirect(url_for("ReleaseView.this_form_get"))

        except Exception as e:
            logging.error(e)
            flash(_("Release failed!"), "danger")
            flash(str(e), "danger")
            return redirect(url_for("ReleaseView.this_form_get"))


appbuilder.add_view(
    ReleaseView,
    "Release",
    label=_("Make a Release"),
    icon="fa-cube",
    category="Release",
    category_label=_("Release"),
)

"""
    Application wide 404 error handler
"""


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )


@appbuilder.app.errorhandler(403)  # FORBIDEN
def forbiden(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        403,
    )


@appbuilder.app.errorhandler(503)  # Service Unavailable
def unavailable(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        503,
    )


# db.create_all()
appbuilder.security_cleanup()
get_dev_transducer()
