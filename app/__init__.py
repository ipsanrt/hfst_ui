# -*- coding: utf-8 -*-

import logging

from flask import Flask
from flask_appbuilder import SQLA, AppBuilder, IndexView
from flask_migrate import Migrate

"""
 Logging configuration
"""

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
logging.getLogger().setLevel(logging.DEBUG)


class MyIndexView(IndexView):
    index_template = "index.html"


app = Flask(__name__)
# app.config.from_object("config")
app.config.from_envvar("FLASK_CONFIG")
db = SQLA(app)
migrate = Migrate(app, db)
appbuilder = AppBuilder(app, db.session, indexview=MyIndexView)


from . import views
