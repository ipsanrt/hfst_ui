import os
import subprocess
import logging

import hfst

from app import app

base_path = app.config["UPLOAD_FOLDER"]
tmp_path = os.path.join(app.config["BASE_DIR"], "tmp/")
twolc_file = os.path.join(tmp_path, "tmp.twolc")
lexc_file = os.path.join(tmp_path, "tmp.lexc")


def run_console_code(code):
    try:
        for cd in code.split("&&"):
            cmd = [c for c in cd.split(" ") if c]
            logging.debug("*** RUNNING: %s" % cmd)
            process = subprocess.Popen(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            stdout, stderr = process.communicate()
            logging.debug("*** STDOUT: %s" % str(stdout))
            logging.debug("*** STDERR: %s" % str(stderr))
            if b"ERROR" in stderr or b"error" in stderr:
                return stdout, stderr

        return stdout, stderr

    except Exception as e:
        logging.debug(e)
        return "", str(e)


# def get_dev_lexc_text():
#     with open(lexc_file, "r", encoding="utf-8") as stream:
#         lexc_text = stream.read()
#     return lexc_text


# def get_dev_twolc_text():
#     with open(twolc_file, "r", encoding="utf-8") as stream:
#         twolc_text = stream.read()
#     return twolc_text


def compile(filename, lexc_text, twolc_text):
    # save lexc and twolc files
    # run compile
    run_console_code("mkdir -p " + base_path)
    run_console_code("mkdir -p " + tmp_path)

    with open(lexc_file, "w", encoding="utf-8") as stream:
        stream.write(lexc_text)
    with open(twolc_file, "w", encoding="utf-8") as stream:
        stream.write(twolc_text)

    params = {
        "base_path": base_path,
        "lexc_file": lexc_file,
        "twolc_file": twolc_file,
        "tmp_path": tmp_path,
        "output_path": os.path.join(base_path, filename),
    }
    code = (
        "rm -f %(base_path)sdev.hfst && "
        "hfst-lexc %(lexc_file)s -o %(tmp_path)stmp.lexc.hfst && "
        "hfst-twolc %(twolc_file)s -o %(tmp_path)stmp.twol.hfst && "
        "hfst-compose-intersect -1 %(tmp_path)stmp.lexc.hfst -2 %(tmp_path)stmp.twol.hfst -o %(tmp_path)stmp.gen.hfst && "
        "hfst-minimize %(tmp_path)stmp.gen.hfst -o %(tmp_path)stmp.gen.min.hfst && "
        "hfst-invert -v %(tmp_path)stmp.gen.min.hfst -o %(tmp_path)stmp.inverted.hfst && "
        "hfst-fst2fst -v %(tmp_path)stmp.inverted.hfst -f olw -o %(output_path)s && "
        "ls %(output_path)s" % params
    )
    logging.debug(code)

    stdout, stderr = run_console_code(code)

    return (1, stdout) if stdout else (0, stderr)


def copy_hfst(initial_hfst_file, output_hfst_file):
    input_file = os.path.join(base_path, initial_hfst_file)
    output_file = os.path.join(base_path, output_hfst_file)
    with open(output_file, "wb") as outpstream:
        with open(input_file, "rb") as inpstream:
            outpstream.write(inpstream.read())


def analyse(word, hfst_file=None):
    if not hfst_file:
        hfst_file = "dev.hfst"
    hfst_file = app.config["UPLOAD_FOLDER"] + hfst_file

    istr = hfst.HfstInputStream(hfst_file)
    transducers = []
    while not (istr.is_eof()):
        transducers.append(istr.read())
    istr.close()
    transducer = transducers[0]

    result = transducer.lookup(word)
    logging.debug("**** WORD: %s" % word)
    logging.debug("**** RESULT: %s" % str(result))

    if result:
        return ";".join([parse for (parse, weight) in result]) + ";"
    else:
        return "NR"
