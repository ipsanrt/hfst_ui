# -*- coding: utf-8 -*-
import datetime

from flask import Markup, url_for, g
from flask_appbuilder import Model
from flask_appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn
from flask_appbuilder.models.decorators import renders
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Text, DateTime
from sqlalchemy.orm import relationship
from flask_babel import lazy_gettext as _

from flask_appbuilder._compat import as_unicode


class Transducer(Model):
    __tablename__ = "transducers"
    id = Column(Integer, primary_key=True)
    tag = Column(String, unique=True, nullable=False)
    lexc_text = Column(Text)
    twolc_text = Column(Text)
    hfst_file = Column(String, unique=True, nullable=False)
    release_date = Column(DateTime, default=datetime.datetime.utcnow)
    user_id = Column(
        Integer, ForeignKey("ab_user.id", onupdate="CASCADE", ondelete="CASCADE"),
    )
    release_note = Column(Text)
    user = relationship("User")

    def __init__(
        self,
        tag=None,
        lexc_text=None,
        twolc_text=None,
        hfst_file=None,
        user=None,
        release_note=None,
    ):
        self.tag = tag
        self.lexc_text = lexc_text
        self.twolc_text = twolc_text
        self.hfst_file = hfst_file
        self.user = user or g.user
        self.release_note = release_note

    def __repr__(self):
        return """%s v%s""" % (_("Transducer"), self.tag)

    @renders("date")
    def date(self):
        if self.release_date:
            return self.release_date.strftime("%d-%m-%Y")
        else:
            return "-"

    @renders("version")
    def version(self):
        return "v%s" % self.tag

    def actions(self):
        return Markup(
            """<div class="btn-group" style="display: flex;">
                 <a class="btn btn-success btn-sm" style="flex: 1;" href="%s"><i class="fa fa-terminal"></i> %s</a>
                 <a class="btn btn-info btn-sm" style="flex: 1;" href="%s"><i class="fa fa-quora"></i> %s</a>
                 <a class="btn btn-warning btn-sm" style="flex: 1;" href="%s"><i class="fa fa-cogs"></i> %s</a>
                 <a class="btn btn-primary btn-sm" style="flex: 1;" href="%s"><i class="fa fa-download"></i> %s</a>
               </div>"""
            % (
                url_for("HFSTAnalyseView.this_form_get", tag=self.tag),
                _("Analyse"),
                url_for("RunTestView.this_form_get", tag=self.tag),
                _("Run Test"),
                url_for("HFSTCompileView.this_form_get", tag=self.tag),
                _("ReCompile"),
                url_for("TransducerView.download", filename=str(self.hfst_file)), # self.hfst_file,
                _("Download"),
            )
        )


class TestCase(Model):
    __tablename__ = "testcases"
    id = Column(Integer, primary_key=True)
    word = Column(String)
    result = Column(String)
    filter_tags = Column(String)

    def __init__(self, word=None, result=None, filter_tags=None):
        self.word = word
        self.result = result
        self.filter_tags = filter_tags

    def __repr__(self):
        return "%s - %s " % (self.word, self.result)

    def tags_list(self):
        html = ""
        if self.filter_tags:
            for tag in self.filter_tags.split(","):
                html += """<span class="badge badge-pill badge-info">%s</span>""" % tag
        return Markup(html)


# class Lexicon(Model):
#     __tablename__ = "lexicons"
#     id = Column(Integer, primary_key=True)
#     lex = Column(String, unique=True, nullable=False)
#     result = Column(String, unique=True, nullable=False)
