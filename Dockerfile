FROM mittelholcz/hfst:hfst-v3.15.0

RUN mkdir /fab
WORKDIR /fab
ADD . /fab
# Web server will listen to this port
EXPOSE 8000
# Install all libraries we saved to requirements.txt file
RUN apk add --update binutils postgresql-libs gcc musl-dev postgresql-dev python3-dev uwsgi-python3 build-base linux-headers pcre-dev
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
